Full-Text RSS Feed Filter
=========================

This is something quick-and-dirty that I knocked together when the
FiveFilters feed-filter installation I had been using decided to quit
working.  It might've been something in the switch to PHP 7.2 on my server,
but their lack of understanding of how open source works (they hide their
latest versions behind a paywall) rubbed me the wrong way.  My replacement
ended up being significantly lighter in weight: a single PHP file.

Usage
-----

Drop fullrss.php into a PHP-enabled webserver somewhere.  Install
[mercury-parser](https://github.com/postlight/mercury-parser); make sure
it's in the webserver's (or PHP's) PATH.  In your feed reader, prepend
"https://your-server-hostname-here/fullrss.php?url=" onto feed URLs that you
want to process.

Articles are cached in /tmp to reduce bandwidth usage and script-processing
time.
