<?php
    // Full RSS Feed
    // replaces article description with article contents
    
    // Copyright 2019 Scott Alfter
    //
    // Permission is hereby granted, free of charge, to any person obtaining
    // a copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to
    // permit persons to whom the Software is furnished to do so, subject to
    // the following conditions:
    //
    // The above copyright notice and this permission notice shall be
    // included in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    // EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    // NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
    // BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
    // ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    // CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    // SOFTWARE.

    // Usage: drop this file onto a PHP-enabled webserver.  Install 
    // mercury-parser (https://github.com/postlight/mercury-parser);
    // make sure it is in the webserver's (or PHP's) PATH.
    // In your feed reader, prepend feed URLs with something like this:
    // https://invalid.example.tld/fullrss.php?url=

    // implement a simple article cache in /tmp
    function fetch_page($url)
    {
        $cachefile="/tmp/fullurl_".hash("sha256", "https://mercury.postlight.com/parser?url=".$url).".html"; // keep this hash for backward compatibility
        
        if (!file_exists($cachefile))
        {
            exec("mercury-parser ".$url, $result);
            foreach ($result as $i)
                if (strpos($i, "\"content\":"))
                    $article=substr($i, strpos($i, "\"content\":")+12, -2);
            $article=str_replace("\\n", "\n", $article);
            file_put_contents($cachefile, $article);
        }
        return file_get_contents($cachefile);
    }
    
    $url=$_GET["url"];
    $feed=new SimpleXmlElement(file_get_contents($url)); // read feed into object

    // type 1
    foreach ($feed->item as $entry) // fix up each entry
    {
        if (substr($entry->link, 0, 21)=="http://minx.cc/?post=") // fix AoSHQ links
            $entry->link="http://minx.cc:1080/?post=".substr($entry->link, 21);
 
        $entry->description=fetch_page($entry->link);
    }

    // type 2
    foreach ($feed->channel->item as $entry) // fix up each entry
        $entry->description=fetch_page($entry->link);

    header("Content-Type: application/rss+xml");
    echo $feed->asXML(); // write the modified feed back out
?>
